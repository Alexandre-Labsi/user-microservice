# Spring - Microservice User

Dans de la cadre de l'intégration de Comiti pour un stage en entreprise de 2 mois, j'ai réçu un test a effectuer.

# Création d'un microservice

J'ai choisi la fonctionnalité 'Utilisateur', implémenté avec en Java avec le Framwork Spring.
Ce microservice utilise une base de donnée Mysql 5.7.

# Mise en place de l'environnement

Grâce à la documentation de Spring, j'ai mis en place le dépendences nécessaires au bon fonctionnement de mon projet.
- Spring web pour la création du service REST
- Lombock pour les annotations getters & setters
- JPA  pour la manipulation des tables
- HSQL pour la liason avec la base de donnée
- OpenFeign pour les requêtes sur un autre microservice

# Développement
Développement de l'organisation du code pour l'implémentation du microservice :
- Model -> un 'Utilisateur' contient un 'pseudo' et un 'mot de passe'.
- Controller -> Récupération, Creation, Suppression, Modification suivant l'url donnée
- Repository -> Méthodes CRUD pour les informations en base de donnée.
- Service -> Autres traitements et vérifications
- Exception -> Lever d'exceptions
- FeignClient -> Client REST qui communique avec un autre microservices REST
