package fr.java.spring.microservices.microtwo.Model;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Entity //hibernate entity
@Data //getters & setters lombock
public class User {

    @Id
    private Long id;
    private String user;
    private String password;
}
