package fr.java.spring.microservices.microtwo.Model;


import lombok.Data;

/**
 * Model de message a recevoir de FeignClient
 */
@Data
public class BeanMessage {

    private Long id;
    private String from;
    private String to;
    private String content;
    private Boolean see;
}
