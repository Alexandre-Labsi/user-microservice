package fr.java.spring.microservices.microtwo.Service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.java.spring.microservices.microtwo.Exception.EmptyException;
import fr.java.spring.microservices.microtwo.Exception.NotFoundException;
import fr.java.spring.microservices.microtwo.Exception.NotModifiedException;
import fr.java.spring.microservices.microtwo.Model.User;
import fr.java.spring.microservices.microtwo.Repository.UserRepository;

@Service
public class UserService {

    @Autowired UserRepository userRepository;

    /**
     * Récupère un bdd tout les utilisateurs
     * @return
     * @throws EmptyException
     */
    public Iterable<User> findAll() throws EmptyException {

        Iterable<User> users = this.userRepository.findAll();
        if(!users.iterator().hasNext()) throw new EmptyException();
        return users;
    }

    /**
     * Crée un utilisateur en bdd
     * @param user
     * @throws NotFoundException
     */
    public User createOne(User user) throws NotFoundException {
        if(user == null) throw new NotFoundException();
        return this.userRepository.save(user);
    }

    /**
     * Edit un user et actualise son status en bdd
     * @param user
     * @throws NotModifiedException
     */
    public User editOne(User user) throws NotModifiedException {

        if(user == null) throw new NotModifiedException();
        //modifier le message
        this.userRepository.save(user);
        return user;
    }

    /**
     * Supprime un user en bdd
     * @param id
     * @return
     * @throws NotFoundException
     */
    public Long deleteOne(Long id) throws NotFoundException {

        if(id == null) throw new NotFoundException();
        this.userRepository.deleteById(id);
        return id;
    }
    
}
