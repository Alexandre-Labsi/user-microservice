package fr.java.spring.microservices.microtwo.Repository;

import org.springframework.data.repository.CrudRepository;

import fr.java.spring.microservices.microtwo.Model.User;

/**
 * Permet l'accès au crud et + pour l'acces en bdd
 */
public interface UserRepository extends CrudRepository<User, Long> {
}
