package fr.java.spring.microservices.microtwo.Controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import antlr.collections.impl.LList;
import ch.qos.logback.core.status.Status;

import javax.websocket.server.PathParam;

import org.apache.coyote.http11.filters.VoidOutputFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import fr.java.spring.microservices.microtwo.FeignClient.MessageClient;
import fr.java.spring.microservices.microtwo.Model.BeanMessage;
import fr.java.spring.microservices.microtwo.Model.User;
import fr.java.spring.microservices.microtwo.Repository.UserRepository;
import fr.java.spring.microservices.microtwo.Service.UserService;
import fr.java.spring.microservices.microtwo.Exception.EmptyException;
import fr.java.spring.microservices.microtwo.Exception.NotFoundException;
import fr.java.spring.microservices.microtwo.Exception.NotModifiedException;


@RestController
public class UserController {

    @Autowired MessageClient messageClient;
    @Autowired UserService userService;

    /**
     * Appel du FeignClient pour récupérer les messages 
     * @return liste total des messages
     * @throws EmptyException
     */
    @GetMapping(path = "/messages")
    private Iterable<BeanMessage> accesToMessage() throws EmptyException {
        return this.messageClient.getMessages();
    }
        
    /**
     * Récupere les users
     * @return les utilisateurs
    * @throws EmptyException
     */
    @GetMapping(path = "/users")
    private Iterable<User> getUsers()throws EmptyException {
        return this.userService.findAll();
    }

    /**
     * Crée un user
     * @param user
     * @return User sauvegarder
    * @throws NotFoundException
     */
    @PostMapping(path = "/login")
    private User login(@RequestBody User user) throws NotFoundException {
        this.userService.createOne(user);
        return user;
    }

    /**
     * Edit un user
     * @param user
     * @return user
     * @throws NotModifiedException
     */
    @PutMapping(path = "/users/edit/")
    private User editUser(User user) throws NotModifiedException {
        this.userService.editOne(user);
        return user;
    }

    /**
     * Supprime un user par son id
     * @return id
     * @param id
     * @throws NotFoundException
     */
    @DeleteMapping(path = "/users/delete/{id}")
    private Long DeleteUser(@PathVariable Long id) throws NotFoundException {
        userService.deleteOne(id);
        return id;
    }
    
}
