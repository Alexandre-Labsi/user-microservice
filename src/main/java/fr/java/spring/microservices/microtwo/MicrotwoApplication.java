package fr.java.spring.microservices.microtwo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class MicrotwoApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicrotwoApplication.class, args);
	}

}
