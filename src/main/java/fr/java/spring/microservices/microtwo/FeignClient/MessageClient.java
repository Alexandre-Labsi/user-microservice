package fr.java.spring.microservices.microtwo.FeignClient;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import fr.java.spring.microservices.microtwo.Model.BeanMessage;

@FeignClient(name = "microservice-message", url = "http://172.17.0.1:9090") //connexion avec microservice-message
public interface MessageClient {
    
    /**
     * Return la liste de messages microservice <Microone>
     */
    @GetMapping(path = "/messages")
    Iterable<BeanMessage>getMessages();
}
